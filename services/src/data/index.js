module.exports = [
  {
    projectId: 1,
    name: "DevOps Fundaments course",
    description: "This is the first module for DevOps branch.",
    startDate: "2020-04-18T00:00:00Z",
    dueDate: "2020-04-26T00:00:00Z"
  },
  {
    projectId: 2,
    name: "Continuous Delivery course",
    description: "This is the second module for DevOps branch.",
    startDate: "2020-05-18T00:00:00Z",
    dueDate: "2020-06-26T00:00:00Z"
  },
  {
    projectId: 3,
    name: "Orchestration course",
    description: "This is the third module for DevOps branch.",
    startDate: "2020-06-18T00:00:00Z",
    dueDate: "2020-07-26T00:00:00Z"
  }
]